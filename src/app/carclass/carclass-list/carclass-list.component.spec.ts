import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarclassListComponent } from './carclass-list.component';

describe('CarclassListComponent', () => {
  let component: CarclassListComponent;
  let fixture: ComponentFixture<CarclassListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarclassListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarclassListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
