import { Component, OnInit } from '@angular/core';
import {Carclass} from "../Carclass";
import {ActivatedRoute, Router} from "@angular/router";
import {CarclassService} from "../CarclassService";

@Component({
  selector: 'app-carclass-list',
  templateUrl: './carclass-list.component.html',
  styleUrls: ['./carclass-list.component.css']
})
export class CarclassListComponent implements OnInit {

  carclasses: Carclass[];
  searchStringValue:string;

  constructor(private carclassService:CarclassService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.loadCarclasses()

  }


  private loadCarclasses(){
    this.carclassService.getCarclass().then(data => this.carclasses = data);
  }


  carclassDeleted(carclass: Carclass, rowIndex: number) {
    let confirm = window.confirm('Soll der Kunde wirklich geloescht werden?');
    if (confirm) {
      this.carclassService.deleteCarclass(carclass.name)
        .then(() => this.carclasses.splice(rowIndex, 1))
        .catch((message) => window.alert(message))
    }
  }

  addCarclass() {
    console.log('add')
    this.router.navigate(['new'], {relativeTo: this.route})
  }

  saveCarclass(carclass: Carclass) {
    this.carclassService.updateCarClass(carclass)
      .then(() => {
          // this.router.navigate(['../..'], {relativeTo: this.route});
        }
      )
      .catch(message => window.alert(message._body));

  }



}
