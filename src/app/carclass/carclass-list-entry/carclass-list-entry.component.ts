import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Carclass} from "../Carclass";
import {CarclassService} from "../CarclassService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-carclass-list-entry',
  templateUrl: './carclass-list-entry.component.html',
  styleUrls: ['./carclass-list-entry.component.css']
})
export class CarclassListEntryComponent implements OnInit {

  showedDetail: boolean;
  markedDirty: boolean;

  @Input() carclass:Carclass;
  @Output('carclassDeleted') changeEvent: EventEmitter<Carclass>;
  @Output('carclassSaved') saveEvent: EventEmitter<Carclass>;

  constructor(private customerService: CarclassService, private router: Router) {
    this.changeEvent = new EventEmitter();
    this.saveEvent = new EventEmitter();
  }

  ngOnInit() {
  }


  deleteCarclass(): void {
    this.changeEvent.emit(this.carclass);

  }

  showDetail() {
    if (this.markedDirty && this.showedDetail) {
      let confirm: boolean = window.confirm("Wollen Sie die Änderungen vor dem Verlassen speichern?");
      console.log("confirm"+confirm);
      if (!confirm) {
        console.log("confirm"+confirm);
        this.loadData();
        this.showedDetail = false;
      } else {
        this.saveCarclass();
        this.showedDetail = !this.showedDetail;
      }
    } else {

      this.showedDetail = !this.showedDetail;
    }
  }

  markDirty() {
    console.log("marktedDirty")
    this.markedDirty = true;
  }

  saveCarclass() {
    console.log("saveCustomer");

    this.customerService.updateCarClass(this.carclass).then(() =>{

      this.loadData()
    }).catch((message) => window.alert(message))

    // this.saveEvent.emit(this.customer);

  }


  private loadData() {

    this.customerService.getCarClassById(this.carclass.name).then((data) => {
      this.carclass = data;
      this.markedDirty = false
    })
  }

}
