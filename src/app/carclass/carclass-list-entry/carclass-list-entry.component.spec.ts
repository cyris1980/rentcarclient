import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarclassListEntryComponent } from './carclass-list-entry.component';

describe('CarclassListEntryComponent', () => {
  let component: CarclassListEntryComponent;
  let fixture: ComponentFixture<CarclassListEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarclassListEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarclassListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
