import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {CustomerService} from "../../customer/customerService";
import {ActivatedRoute, Router} from "@angular/router";
import {Carclass} from "../Carclass";
import {CarclassService} from "../CarclassService";

@Component({
  selector: 'app-carclass-form',
  templateUrl: './carclass-form.component.html',
  styleUrls: ['./carclass-form.component.css']
})
export class CarclassFormComponent implements OnInit {

  form;

  constructor(private formBuilder: FormBuilder,
              private carclassService: CarclassService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      name: this.formBuilder.control(''),
      dayRate: this.formBuilder.control('')
    })
  }

  onSubmit(carclass:Carclass) {
    this.carclassService.saveCarclass(carclass).then(() =>

      this.navigateBack()
    );
  }

  private navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
