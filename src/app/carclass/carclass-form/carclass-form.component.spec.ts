import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarclassFormComponent } from './carclass-form.component';

describe('CarclassFormComponent', () => {
  let component: CarclassFormComponent;
  let fixture: ComponentFixture<CarclassFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarclassFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarclassFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
