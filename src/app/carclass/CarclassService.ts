/**
 * Created by cyrill on 14.07.17.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import {Carclass} from "./Carclass";




@Injectable()
export class CarclassService {

  private url = 'http://185.142.212.45:8080/rentcar/services/carClass';

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error);
  }

  constructor(private http: Http) {
  }


  getCarclass(): Promise<Carclass[]> {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders});

    return this.http
      .get(this.url, options)
      .toPromise()
      .then(response => {
        return response.json() as Carclass[];
      })
      .catch(this.handleError);

  }

  saveCarclass(carclass:Carclass): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    return this.http
      .post(this.url, carclass, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  deleteCarclass(id: string): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  getCarClassById(id: string): Promise<Carclass> {
    const url = `${this.url}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => {
        return response.json() as Carclass;
      })
      .catch(this.handleError);

  }

  updateCarClass(carclass: Carclass): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    const url = `${this.url}/${carclass.name}`;
    return this.http
      .put(url, carclass, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError)

  }




  private getRequestOptions(): RequestOptions {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json;charset=utf-8');
    return new RequestOptions({headers: myHeaders});
  }
}
