import {CarclassListComponent} from "./carclass-list/carclass-list.component";
import {CarclassFormComponent} from "./carclass-form/carclass-form.component";

export const carclassRoutes = [
  {
    path: '',
    children: [
      {path: '', component: CarclassListComponent},
      {path: 'new', component: CarclassFormComponent},

    ]
  }];

export const carclassRoutingComponents = [CarclassListComponent, CarclassFormComponent];
