export class Carclass{

  private _dayRate: number;

  private _name: string;

  constructor(){

    this._dayRate = 0;
    this._name = "";

  }


  get dayRate(): number {
    return this._dayRate;
  }

  set dayRate(value: number) {
    this._dayRate = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}
