import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {customerRoutes} from "./customer/customerRouting";
import {reservationRoutes} from "./reservation/reservationRouting";
import {carclassRoutes} from "./carclass/CarclassRouting";
import {carRoutes} from "./cars/CarRouting";

/**
 * Created by cyrill on 15.07.17.
 */

const appRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'customers', children: customerRoutes},
  {path: 'reservation', children: reservationRoutes},
  {path: 'carclass', children: carclassRoutes},
  {path: 'car', children: carRoutes}
];


export const routing = RouterModule.forRoot(appRoutes);
