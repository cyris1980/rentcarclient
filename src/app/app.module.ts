import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {CustomerService} from "./customer/customerService";
import {DashboardComponent} from './dashboard/dashboard.component';
import {routing} from "./routing";
import {customerRoutingComponents} from "./customer/customerRouting";
import { MyDatePickerModule } from 'mydatepicker';
import {reservationRoutingComponents} from "./reservation/reservationRouting";
import {ReservationService} from "./reservation/ReservationService";
import {MdDatepickerModule} from "@angular/material";
import { CarclassListComponent } from './carclass/carclass-list/carclass-list.component';
import { CarclassFormComponent } from './carclass/carclass-form/carclass-form.component';
import { CarclassListEntryComponent } from './carclass/carclass-list-entry/carclass-list-entry.component';
import {CarclassService} from "./carclass/CarclassService";
import { CarsListEntryComponent } from './cars/cars-list-entry/cars-list-entry.component';
import { CarsListComponent } from './cars/cars-list/cars-list.component';
import { CarsFormComponent } from './cars/cars-form/cars-form.component';
import {CarService} from "./cars/CarService";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    customerRoutingComponents,
    reservationRoutingComponents,
    CarclassListComponent,
    CarclassFormComponent,
    CarclassListEntryComponent,
    CarsListEntryComponent,
    CarsListComponent,
    CarsFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    MyDatePickerModule,
    MdDatepickerModule
  ],
  providers: [CustomerService, ReservationService, CarclassService, CarService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
