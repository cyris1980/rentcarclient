/**
 * Created by cyrill on 14.07.17.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";


import {Reservation} from "./Reservation";


@Injectable()
export class ReservationService {

  private url = 'http://185.142.212.45:8080/rentcar/services/reservation';

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error);
  }

  constructor(private http: Http) {
  }


  getReservations(): Promise<Reservation[]> {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders});

    return this.http
      .get(this.url, options)
      .toPromise()
      .then(response => {
        return response.json() as Reservation[];
      })
      .catch(this.handleError);

  }

  saveReservation(reservation): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    return this.http
      .post(this.url, reservation, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  deleteReservation(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  getReservationByNr(id: number): Promise<Reservation> {
    const url = `${this.url}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => {
        return response.json() as Reservation;
      })
      .catch(this.handleError);

  }

  updateReservation(reservation: Reservation): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    const url = `${this.url}/${reservation.reservationNo}`;
    return this.http
      .put(url, reservation, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError)

  }


  private getRequestOptions(): RequestOptions {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json;charset=utf-8');
    return new RequestOptions({headers: myHeaders});
  }

  searchReservation(searchString:string): Promise<Reservation[]>{
    const searchParams = new URLSearchParams();
    searchParams.append('searchString',searchString);

    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders, search: searchParams});

    return this.http
      .get(this.url+"/searchByCustomerSearchString?customerSearchString="+searchString, {search: searchParams})
      .toPromise()
      .then(response => {
        return response.json() as Reservation[];
      })
      .catch(this.handleError);

  }
}
