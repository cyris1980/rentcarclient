import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ReservationService} from "../ReservationService";
import {ActivatedRoute, Router} from "@angular/router";
import {Reservation} from "../Reservation";
import {CustomerService} from "../../customer/customerService";
import {Carclass} from "../../carclass/Carclass";
import {CarclassService} from "../../carclass/CarclassService";

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.css']
})
export class ReservationFormComponent implements OnInit {

  form;
  public carclasses:Carclass[];


  constructor(private formBuilder: FormBuilder,
              private reservationService: ReservationService,
              private customerService:CustomerService,
              private carclassService:CarclassService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      carClassName: this.formBuilder.control(''),
      carIdent: this.formBuilder.control(''),
      customerNo: this.formBuilder.control(''),
      startDate: [null, Validators.required],
      endDate: this.formBuilder.control(''),
      totalCost: this.formBuilder.control('')

    });
    this.carclassService.getCarclass().then(carclasses => this.carclasses = carclasses);

    this.form.controls['customerNo'].setValue(this.customerService.getCustomerNo());
  }


  onSubmit(reservation) {
    this.reservationService.saveReservation(reservation)
      .then(() =>

      this.navigateBack()
    );
  }

  private navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  calculateTotalCost(){

    let carClass = this.form.controls['carClassName'].value
    let startDates:Date = new Date(this.form.controls['startDate'].value);
    let endDates:Date = new Date(this.form.controls['endDate'].value);

    if(carClass && startDates && endDates){

      let find:Carclass = this.carclasses.find(c => c.name == carClass);
      let startDay:number = startDates.getTime();
      let endDay:number = endDates.getTime();
      let days = ((endDay - startDay) / (60 * 60 * 24 * 1000)) + 1;
      let calculatedCosts = days * find.dayRate;
      this.form.controls['totalCost'].setValue(calculatedCosts);

    }


  }


}
