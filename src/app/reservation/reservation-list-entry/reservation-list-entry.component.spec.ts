import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationListEntryComponent } from './reservation-list-entry.component';

describe('ReservationListEntryComponent', () => {
  let component: ReservationListEntryComponent;
  let fixture: ComponentFixture<ReservationListEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationListEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
