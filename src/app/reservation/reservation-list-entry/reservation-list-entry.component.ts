import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReservationService} from "../ReservationService";
import {ActivatedRoute, Router} from "@angular/router";
import {MdDatepickerModule} from '@angular/material';
import {Reservation} from "../Reservation";
import {IMyDpOptions} from "mydatepicker";
import {CarclassService} from "../../carclass/CarclassService";
import {Carclass} from "../../carclass/Carclass";
import {Car} from "../../cars/Car";
import {CarService} from "../../cars/CarService";

@Component({
  selector: 'app-reservation-list-entry',
  templateUrl: './reservation-list-entry.component.html',
  styleUrls: ['./reservation-list-entry.component.css']
})
export class ReservationListEntryComponent implements OnInit {

  showedDetail: boolean;
  markedDirty: boolean;
  carclasses: Carclass[];
  cars: Car[];


  @Input() reservation;
  @Output('reservationDelete') changeEvent: EventEmitter<Reservation>;
  @Output('reservationSave') saveEvent: EventEmitter<Reservation>;

  constructor(private reservationService:ReservationService,
              private carclassService:CarclassService,
              private carService:CarService) {
    this.changeEvent = new EventEmitter();
    this.saveEvent = new EventEmitter();
  }

  ngOnInit() {

    this.carclassService.getCarclass().then(carclasses => this.carclasses = carclasses);
    this.carService.searchCarByCarClass(this.reservation.carClassName).then(cars => this.cars = cars);

  }

  deleteReservation(): void {
    this.changeEvent.emit(this.reservation.reservationNo);

  }

  showDetail() {
    if (this.markedDirty && this.showedDetail) {
      let confirm: boolean = window.confirm("Ungespeicherte Daten. Wollen sie wirklich die Anzeige verlassen?");
      if (!confirm) {
      } else {
        this.showedDetail = !this.showedDetail;
      }
    } else {

      this.showedDetail = !this.showedDetail;
    }
  }

  markDirty() {
    this.markedDirty = true;
  }

  saveReservation() {

    // this.saveEvent.emit(this.reservation);
    this.reservationService.updateReservation(this.reservation).then(()=>
      this.reservationService.getReservationByNr(this.reservation.reservationNo).then((data => this.reservation = data)));
    this.markedDirty = false;

  }
  changeCarClass(){
    this.carService.searchCarByCarClass(this.reservation.carClassName).then(cars => this.cars = cars);
    this.markDirty();

  }

}
