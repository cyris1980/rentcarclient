/**
 * Created by cyrill on 16.07.17.
 */

import {ReservationListComponent} from "./reservation-list/reservation-list.component";
import {ReservationFormComponent} from "./reservation-form/reservation-form.component";
import {ReservationListEntryComponent} from "./reservation-list-entry/reservation-list-entry.component";

export const reservationRoutes = [
  {
    path: '',
    children: [
      {path: '', component: ReservationListComponent},
      {path: 'new', component: ReservationFormComponent},

    ]
  }];

export const reservationRoutingComponents = [ReservationListComponent, ReservationFormComponent, ReservationListEntryComponent];
