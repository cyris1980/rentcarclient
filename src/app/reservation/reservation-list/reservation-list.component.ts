import {Component, OnInit} from '@angular/core';
import {Reservation} from "../Reservation";
import {ReservationService} from "../ReservationService";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  reservations: Reservation[];
  searchStringValue: string;
  searchTime: Date;

  constructor(private reservationService: ReservationService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.loadReservations();
  }

  private loadReservations() {
    this.reservationService.getReservations()
      .then(data => {
        this.reservations = data;
      })

  }

  deleteReservation(reservation: Reservation, rowIndex: number) {
    let confirm = window.confirm('Soll die Reservation wirklich geloescht werden?');
    if (confirm) {
      this.reservationService.deleteReservation(reservation.reservationNo)
        .then(() => this.reservations.splice(rowIndex, 1))
        .catch((message) => window.alert(message))
    }
  }

  addReservation() {
    console.log('addReservation')
    this.router.navigate(['customers'])
  }

  saveReservation(reservation: Reservation) {
    this.reservationService.updateReservation(reservation)
      .then(
        // () => this.loadReservations()
      )
      .catch(message => window.alert(message._body));

  }

  searchReservation() {

    if (this.searchStringValue.length > 0) {
      this.filterReservations();

    } else {
      this.loadReservations()
    }

  }

  filterReservations() {
    this.reservations = this.reservations.filter(reservation => {

      if (Number.parseInt(this.searchStringValue) == reservation.reservationNo) {
        return true;
      }
      if (reservation.customerName.includes(this.searchStringValue)) {
        return true;
      }
      if (reservation.customerFirstname.includes(this.searchStringValue)) {
        return true;
      }
      return false;
    })

  }

}
