export class Reservation {

  public carClassName: string;

  public carIdent: string;

  public customerNo: number;

  public endDate: Date;

  public reservationNo: number;

  public startDate: Date;

  public totalCost: number;

  public customerFirstname: string;

  public customerName:string;

  constructor(){

    this.carClassName = "";

    this.carIdent = "";

    this.customerNo = 0;

    this.endDate = new Date();

    this.reservationNo = 0;

    this.startDate = new Date();

    this.totalCost = 0;

    this.customerFirstname = "";

    this.customerName = "";

  }

}
