import {Address} from "./Address";
import {isUndefined} from "util";
/**
 * Created by cyrill on 15.07.17.
 */

export class Customer {

  public address: Address;

  public firstname: string;

  public name: string;

  public number: number;

  constructor(){
    this.address = new Address;
    this.firstname = "";
    this.name = "";
    this.number = 0;
  }

  public setAddress(address: Address){
    this.address = address;
  }
}
