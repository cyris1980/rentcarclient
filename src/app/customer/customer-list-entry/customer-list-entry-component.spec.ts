import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerListEntryComponentComponent } from './customer-list-entry-component';

describe('CustomerListEntryComponentComponent', () => {
  let component: CustomerListEntryComponentComponent;
  let fixture: ComponentFixture<CustomerListEntryComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerListEntryComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerListEntryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
