import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Customer} from "../Customer";
import {CustomerService} from "../customerService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-customer-list-entry-component',
  templateUrl: './customer-list-entry-component.html',
  styleUrls: ['./customer-list-entry-component.css']
})
export class CustomerListEntryComponentComponent implements OnInit {

  showedDetail: boolean;
  markedDirty: boolean;


  @Input() customer;
  @Output('customerDeleted') changeEvent: EventEmitter<Customer>;
  @Output('customerSaved') saveEvent: EventEmitter<Customer>;

  constructor(private customerService: CustomerService, private router: Router) {
    this.changeEvent = new EventEmitter();
    this.saveEvent = new EventEmitter();
  }

  ngOnInit() {

  }


  deleteCustomer(): void {
    this.changeEvent.emit(this.customer.number);

  }

  showDetail() {
    if (this.markedDirty && this.showedDetail) {
      let confirm: boolean = window.confirm("Wollen Sie die Änderungen vor dem Verlassen speichern?");
      console.log("confirm"+confirm);
      if (!confirm) {
        console.log("confirm"+confirm);
        this.loadData();
        this.showedDetail = false;
      } else {
        this.saveCustomer();
        this.showedDetail = !this.showedDetail;
      }
    } else {

      this.showedDetail = !this.showedDetail;
    }
  }

  markDirty() {
    console.log("marktedDirty")
    this.markedDirty = true;
  }

  saveCustomer() {
    console.log("saveCustomer");

    this.customerService.updateCustomer(this.customer).then(() =>{

        this.loadData()
    }).catch((message) => window.alert(message))

    // this.saveEvent.emit(this.customer);

  }

  makeReservation() {
    this.customerService.addCustonerNo(this.customer.number);
    this.router.navigate(['reservation/new']);
  }

  private loadData() {

    this.customerService.getCustomerByNr(this.customer.number).then((data) => {
      this.customer = data;
      this.markedDirty = false
    })
  }


}
