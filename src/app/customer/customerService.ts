/**
 * Created by cyrill on 14.07.17.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";


import {Customer} from "./Customer";


@Injectable()
export class CustomerService {

  private customersUrl = 'http://185.142.212.45:8080/rentcar/services/customers';
  private customerNo: number;

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error);
  }

  constructor(private http: Http) {
  }


  getCustomers(): Promise<Customer[]> {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders});

    return this.http
      .get(this.customersUrl, options)
      .toPromise()
      .then(response => {
        return response.json() as Customer[];
      })
      .catch(this.handleError);

  }

  saveCustomer(customer): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    console.log("addCustoemr");
    return this.http
      .post(this.customersUrl, customer, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  deleteCustomer(id: number):Promise<void> {
    const url = `${this.customersUrl}/${id}`;
    return this.http
      .delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  getCustomerByNr(id: number): Promise<Customer> {
    console.log("getCustomerByNr");
    const url = `${this.customersUrl}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => {
        return response.json() as Customer;
      })
      .catch(this.handleError);

  }

  updateCustomer(customer: Customer): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    const url = `${this.customersUrl}/${customer.number}`;
    return this.http
      .put(url, customer, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError)

  }

  searchCustomer(searchString:string): Promise<Customer[]>{
    const searchParams = new URLSearchParams();
    searchParams.append('searchString',searchString);

    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders, search: searchParams});

    return this.http
      .get(this.customersUrl+"/searchByString?searchString="+searchString, {search: searchParams})
      .toPromise()
      .then(response => {
        return response.json() as Customer[];
      })
      .catch(this.handleError);

  }

  public addCustonerNo(no:number){
    this.customerNo = no;
  }

  public getCustomerNo():number{
    let no = this.customerNo;
    this.customerNo = null;
    return no;
  }

  private getRequestOptions(): RequestOptions {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json;charset=utf-8');
    return new RequestOptions({headers: myHeaders});
  }
}
