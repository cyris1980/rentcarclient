import {CustomerListComponentComponent} from "./customer-list/customer-list-component";
import {CustomerEditComponent} from "./customer-edit/customer-edit.component";
import {CustomerFormComponent} from "./customer-new/customer-new.component";
import {CustomerListEntryComponentComponent} from "./customer-list-entry/customer-list-entry-component";
/**
 * Created by cyrill on 16.07.17.
 */
export const customerRoutes = [
  {
    path: '',
    children: [
      {path: '', component: CustomerListComponentComponent},
      {path: 'showDetail/:id', component: CustomerEditComponent},
      {path: 'new', component: CustomerFormComponent},

    ]
  }];

export const customerRoutingComponents = [CustomerListComponentComponent, CustomerListEntryComponentComponent, CustomerFormComponent, CustomerEditComponent];
