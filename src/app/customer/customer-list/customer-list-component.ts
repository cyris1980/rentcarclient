import {Component, OnInit} from '@angular/core';
import {CustomerService} from "../customerService";
import {Customer} from "../Customer";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-customer-list-component',
  templateUrl: './customer-list-component.html',
  styleUrls: ['./customer-list-component.css']
})
export class CustomerListComponentComponent implements OnInit {

  customers: Customer[];
  searchStringValue: string;

  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.loadCustomers();
  }

  private loadCustomers() {
    this.customerService.getCustomers().then(data => {
      this.customers = data;

    });
  }

  customerDeleted(customer: Customer, rowIndex: number) {
    let confirm = window.confirm('Soll der Kunde wirklich geloescht werden?');
    if (confirm) {
      this.customerService.deleteCustomer(customer.number)
        .then(() => this.customers.splice(rowIndex, 1))
        .catch((message) => window.alert(message))
    }
  }

  addCustomer() {
    console.log('add')
    this.router.navigate(['new'], {relativeTo: this.route})
  }

  saveCustomer(customer: Customer) {
    this.customerService.updateCustomer(customer)
      .then(() => {
          // this.router.navigate(['../..'], {relativeTo: this.route});
        }
      )
      .catch(message => window.alert(message._body));

  }

  searchCustomer() {
    console.log("searchCustomer with: " + this.searchStringValue);
    if (this.searchStringValue.length > 0) {
      this.filterCustomers();

    } else {
      this.loadCustomers()
    }

  }

  filterCustomers() {
    this.customers = this.customers.filter(customer => {

      if (Number.parseInt(this.searchStringValue) == customer.number) {
        return true;
      }
      if (customer.name.includes(this.searchStringValue)) {
        return true;
      }
      if (customer.firstname.includes(this.searchStringValue)) {
        return true;
      }
      return false;
    })

  }


}
