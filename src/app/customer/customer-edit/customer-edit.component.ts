import {Component, Input, OnInit} from "@angular/core";
import {CustomerService} from "../customerService";
import {Customer} from "../Customer";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import "rxjs/add/operator/switchMap";
import {InputDecorator} from "@angular/core/src/metadata/directives";
import {Address} from "../Address";


@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

  customer: Customer;


  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private router: Router,) {
  }

  ngOnInit() {
    this.customer = new Customer;
    this.customer.setAddress(new Address());

    this.route.paramMap
      .switchMap((params: ParamMap) => this.customerService.getCustomerByNr(+params.get(('id'))))
      .subscribe(cust => {
        this.customer = cust
      });


  }

  saveCustomer() {
    this.customerService.updateCustomer(this.customer)
      .then(() => {
        this.router.navigate(['../..'], {relativeTo: this.route});
      }
    )
      .catch(message => window.alert(message._body));

  }

}
