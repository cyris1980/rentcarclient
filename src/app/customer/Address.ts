/**
 * Created by cyrill on 15.07.17.
 */
export class Address {

  public city: string;
  public country: string;
  public postZip: string;
  public street: string;

  constructor(){
    this.city = "";
    this.country = "";
    this.postZip = "";
    this.street = "blub";
  }
}
