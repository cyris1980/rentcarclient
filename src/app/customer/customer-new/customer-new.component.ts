import {Component, OnInit} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {CustomerService} from "../customerService";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-new.component.html',
  styleUrls: ['./customer-new.component.css']
})
export class CustomerFormComponent implements OnInit {

  form;

  constructor(private formBuilder: FormBuilder,
              private customerService: CustomerService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      number: this.formBuilder.control(''),
      firstname: this.formBuilder.control(''),
      name: this.formBuilder.control(''),
      city: this.formBuilder.control(''),
      postZip: this.formBuilder.control(''),
      country: this.formBuilder.control(''),
      street: this.formBuilder.control('')
    })
  }

  onSubmit(customer) {
    console.log(customer);
    let addr = {
      "city": customer.city,
      "country": customer.country,
      "postZip": customer.postZip,
      "street": customer.street
    };


    let cust = {
      'address': addr,
      'firstname': customer.firstname,
      'name': customer.name,
      'number': customer.number

    }

    this.customerService.saveCustomer(cust).then(() =>

      this.navigateBack()
    );
  }

  private navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
