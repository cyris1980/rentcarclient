import { Component, OnInit } from '@angular/core';
import {Car} from "../Car";
import {CarService} from "../CarService";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css']
})
export class CarsListComponent implements OnInit {

  cars: Car[];
  searchStringValue:string;

  constructor(private carService:CarService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.loadCars()

  }


  private loadCars(){
    this.carService.getCars().then(data => this.cars = data);
  }


  carDeleted(car: Car, rowIndex: number) {
    let confirm = window.confirm('Soll der Car wirklich geloescht werden?');
    if (confirm) {
      this.carService.deleteCar(car.identification)
        .then(() => this.cars.splice(rowIndex, 1))
        .catch((message) => window.alert(message))
    }
  }

  addCar() {
    console.log('add')
    this.router.navigate(['new'], {relativeTo: this.route})
  }

  saveCar(car: Car) {
    this.carService.updateCar(car)
      .then(() => {

        }
      )
      .catch(message => window.alert(message._body));

  }

  searchCar() {

    if (this.searchStringValue.length > 0) {
      this.filterCar();

    } else {
      this.loadCars()
    }

  }

  filterCar() {
    this.cars = this.cars.filter(car => {


      if (car.identification.includes(this.searchStringValue)) {
        return true;
      }
      if (car.carClassName.includes(this.searchStringValue)) {
        return true;
      }
      return false;
    })

  }



}
