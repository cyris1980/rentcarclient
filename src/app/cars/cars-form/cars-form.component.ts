import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {CarService} from "../CarService";
import {ActivatedRoute, Router} from "@angular/router";
import {Car} from "../Car";
import {Carclass} from "../../carclass/Carclass";
import {CarclassService} from "../../carclass/CarclassService";

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  form;
  carclasses:Carclass[];

  constructor(private formBuilder: FormBuilder,
              private carService: CarService,
              private carClassService:CarclassService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      carClassName: this.formBuilder.control(''),
      brand: this.formBuilder.control(''),
      identification: this.formBuilder.control(''),
      typ: this.formBuilder.control('')
    });
    this.carClassService.getCarclass().then(classes => this.carclasses = classes);
  }

  onSubmit(car:Car) {
    this.carService.saveCar(car).then(() =>

      this.navigateBack()
    );
  }

  private navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
