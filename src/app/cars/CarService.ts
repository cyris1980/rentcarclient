
import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";
import {Car} from "./Car";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import {Carclass} from "../carclass/Carclass";

@Injectable()
export class CarService{

  private url = 'http://185.142.212.45:8080/rentcar/services/car';

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error);
  }

  constructor(private http: Http) {
  }


  getCars(): Promise<Car[]> {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders});

    return this.http
      .get(this.url, options)
      .toPromise()
      .then(response => {
        return response.json() as Car[];
      })
      .catch(this.handleError);

  }

  saveCar(car:Car): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    return this.http
      .post(this.url, car, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  deleteCar(id: string): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  getCarById(id: string): Promise<Car> {
    const url = `${this.url}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => {
        return response.json() as Car;
      })
      .catch(this.handleError);

  }

  updateCar(car: Car): Promise<void> {
    let requestOptions: RequestOptions = this.getRequestOptions();
    const url = `${this.url}/${car.identification}`;
    return this.http
      .put(url, car, requestOptions)
      .toPromise()
      .then(() => null)
      .catch(this.handleError)

  }

  searchCarByCarClass(searchString:string): Promise<Car[]> {
    const searchParams = new URLSearchParams();
    searchParams.append('searchString', searchString);

    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Accept-Language', 'de');

    let options = new RequestOptions({headers: myHeaders, search: searchParams});

    return this.http
      .get(this.url + "/carsByCarClassName?carClassName=" + searchString, {search: searchParams})
      .toPromise()
      .then(response => {
        return response.json() as Car[];
      })
      .catch(this.handleError);
  }

  private getRequestOptions(): RequestOptions {
    let myHeaders = new Headers();
    myHeaders.set('Content-Type', 'application/json;charset=utf-8');
    return new RequestOptions({headers: myHeaders});
  }

}
