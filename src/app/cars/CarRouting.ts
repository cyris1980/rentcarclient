

import {CarsListComponent} from "./cars-list/cars-list.component";
import {CarsFormComponent} from "./cars-form/cars-form.component";

export const carRoutes = [
  {
    path: '',
    children: [
      {path: '', component: CarsListComponent},
      {path: 'new', component: CarsFormComponent},

    ]
  }];

export const carRoutingComponents = [CarsListComponent, CarsFormComponent];
