export class Car {

  public brand:string;

  public carClassName:string;

  public identification: string;

  public typ:string;


  constructor() {
    this.carClassName = '';
    this.brand = '';
    this.identification = '';
    this.typ = '';
  }
}
