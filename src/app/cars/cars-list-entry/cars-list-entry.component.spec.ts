import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsListEntryComponent } from './cars-list-entry.component';

describe('CarsListEntryComponent', () => {
  let component: CarsListEntryComponent;
  let fixture: ComponentFixture<CarsListEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsListEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
