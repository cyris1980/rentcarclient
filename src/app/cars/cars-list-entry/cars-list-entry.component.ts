import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Car} from "../Car";
import {CarService} from "../CarService";
import {Router} from "@angular/router";
import {CarclassService} from "../../carclass/CarclassService";
import {Carclass} from "../../carclass/Carclass";

@Component({
  selector: 'app-cars-list-entry',
  templateUrl: './cars-list-entry.component.html',
  styleUrls: ['./cars-list-entry.component.css']
})
export class CarsListEntryComponent implements OnInit {


  showedDetail: boolean;
  markedDirty: boolean;
  carclasses:Carclass[];

  @Input() car:Car;
  @Output('carDeleted') changeEvent: EventEmitter<Car>;
  @Output('carSaved') saveEvent: EventEmitter<Car>;

  constructor(private carService: CarService, private router: Router, private carClassService:CarclassService) {
    this.changeEvent = new EventEmitter();
    this.saveEvent = new EventEmitter();
  }

  ngOnInit() {
    this.carClassService.getCarclass().then(classes => this.carclasses = classes);
  }


  deleteCar(): void {
    this.changeEvent.emit(this.car);

  }

  showDetail() {
    if (this.markedDirty && this.showedDetail) {
      let confirm: boolean = window.confirm("Wollen Sie die Änderungen vor dem Verlassen speichern?");
      console.log("confirm"+confirm);
      if (!confirm) {
        console.log("confirm"+confirm);
        this.loadData();
        this.showedDetail = false;
      } else {
        this.saveCar();
        this.showedDetail = !this.showedDetail;
      }
    } else {

      this.showedDetail = !this.showedDetail;
    }
  }

  markDirty() {
    console.log("marktedDirty")
    this.markedDirty = true;
  }

  saveCar() {
    console.log("saveCustomer");

    this.carService.updateCar(this.car).then(() =>{

      this.loadData()
    }).catch((message) => window.alert(message))

  }


  private loadData() {

    this.carService.getCarById(this.car.identification).then((data) => {
      this.car = data;
      this.markedDirty = false
    })
  }

}
