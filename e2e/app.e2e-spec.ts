import { RentCarPage } from './app.po';

describe('rent-car App', () => {
  let page: RentCarPage;

  beforeEach(() => {
    page = new RentCarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
